# HDCore - docker-pylint

## Introduction

This is a small container image that contains a python3 environment with yamllint1. This container is very usefull for automatic testing during CI.

## Image usage

- Run yamllint:

```bash
docker run --rm -v /path/to/python/code:/code hdcore/docker-yamllint:<version> yamllint <arguments>
```

- Run shell:

```bash
docker run -it --rm -v /path/to/python/code:/code hdcore/docker-yamllint:<version> /bin/sh
```

- Use in .gitlab-ci.yml:

```bash
image: hdcore/docker-yamllint:<version>
script: yamllint <arguments>
```

## Available tags

- hdcore/docker-yamllint:1

## Container Registries

The image is stored on multiple container registries at dockerhub and gitlab:

- Docker Hub:
  - hdcore/docker-yamllint
  - registry.hub.docker.com/hdcore/docker-yamllint
- Gitlab:
  - registry.gitlab.com/hdcore-docker/docker-yamllint

## Building image

Build:

```bash
docker build -f <version>/Dockerfile -t docker-yamllint:<version> .
```
