#!/bin/sh

# Author: Danny Herpol <hdcore@lachjekrom.com>
# Version GIT: 2021-11-13 17:21

# docker-entrypoint.sh
# for hdcore docker-yamllint image

CGREEN="\e[32m"
CNORMAL="\e[0m"

# shellcheck disable=SC2059
printf "== ${CGREEN}Start entrypoint ${0}${CNORMAL} ==\n"

printf "HDCore docker-yamllint container image\n"
printf "Installed version of yamllint:\n"
yamllint --version

# shellcheck disable=SC2059
printf "== ${CGREEN}End entrypoint ${0}${CNORMAL} ==\n"

# Execute docker CMD
exec "$@"